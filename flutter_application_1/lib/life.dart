import 'package:flutter/material.dart';

class Cycle extends StatefulWidget {
  const Cycle({super.key});

  State<Cycle> createState() {
    print("In createfunction");
    return CycleStat();
  }
}

class CycleStat extends State<Cycle> {
  void initState() {
    print("In_initstate");
    super.initState();
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    print("DidchangeDependancies");
  }

  void deactivate() {
    super.deactivate();
    print("deactivate");
  }

  void dispose() {
    super.dispose();
    print("dispose");
  }

  bool lifecycle = true;

  Scaffold Widgetlife() {
    if (lifecycle == true) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.teal,
          title: const Text(
            "Welcome",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        body: Center(
          child: Text("if condition"),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              lifecycle = false;
            });
          },
          child: Text("click"),
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.teal,
          title: const Text(
            "Welcome",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        body: Center(
          child: Text("else condition"),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              lifecycle = true;
            });
          },
          child: Text("click"),
        ),
      );
    }
  }

  Widget build(BuildContext context) {
    print("Widgetlife");
    return Widgetlife();
  }
}
//
